@extends('layouts.admin')

@section('content')
<div class="tile">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="caption">
                    <h2 style="text-align: center">Ajout d'images</h2>
                </div>
                <div class="ml-auto">
                    <form method="POST" action="{{route('admin.ajout-slide')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="image">
                            <button class="btn btn-info" type="submit">Valider</button>
                    </form>

                </div>
            </div>


        </div>
        <div class="card-body">
            <div class="table-scrollable table-responsive">

    <table class="table table-hover" id="datTable">
        <thead>
            <tr>
                <th>
                    Images
                </th>
                <th class="ml-auto">
                   Actions
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($slides as $slide)
            <tr>
                <td>
                    <img src="http://localhost:8001/ascad/{{$slide->image}}" alt="slide" class="card" width="200" height="150">
                </td>
                <td>
                    <a href="{{route('admin.supprimer', $slide->id)}}" class="btn btn-danger btn-sm">
                        Supprimer l'image
                    </a>
                </td>
                </tr>
                @endforeach
                <tbody>
                </table>
               {{--  {{$users->links()}} --}}

            </div>
        </div>
    </div>
</div>

            @endsection