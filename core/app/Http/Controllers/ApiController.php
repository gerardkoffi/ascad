<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
       public function slideIndex()
    {
        $slides= Slide::limit(4)->get();
        return response()->json([
            'success' => true,
            'message' => 'Slide',
            'data' =>$slides,
        ]);
    }

     public function login(Request $request)
    {

        if (Auth::guard('admin')->attempt(['username' => $request->username,
            'password' => $request->password]))
        {
            session()->put('site_id',$request->id);
            return response()->json([
            'success' => true,
            'message' => 'ok',
            'data' =>[$request->username,$request->password,$request->id],
        ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Le nom d\'utilisateur et le mot de passe ne correspondent pas',
        ]);
    }
}
